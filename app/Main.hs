module Main where

import Problems
import Lib

main :: IO ()
main = do 
    print (aoc_loesung_1_1_0 aoc_daten_1_1_0)
    print (aoc_loesung_1_2_0 aoc_daten_1_1_0)
    print (aoc_loesung_2_1_0 aoc_daten_2_1_0 aoc_daten_2_2_0)
    print (aoc_loesung_2_2_0 aoc_daten_2_1_0 aoc_daten_2_2_0)
    print (aoc_loesung_3_1_0 aoc_daten_3_1_0)
    print (aoc_loesung_3_2_0 aoc_daten_3_1_0)
