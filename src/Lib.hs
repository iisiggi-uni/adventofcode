module Lib ( 
    aoc_loesung_1_1_0,
    aoc_loesung_1_2_0,
    aoc_loesung_2_1_0,
    aoc_loesung_2_2_0,
    aoc_loesung_3_1_0,
    aoc_loesung_3_2_0
) where


import Model 


{-  
    Lösung der ersten Aufgabe im Adventskalender 
-}
aoc_loesung_1_1_0:: [Tiefe] -> Int
aoc_loesung_1_1_0 tiefen
    -- wenn genug elemente zum vergleichen vorhanden sind vergleiche sie
    | length tiefen >= 2 = vergleiche tiefen
    | otherwise = 0
    where
        -- zähle wie häufig die tiefe steigt
        vergleiche (tiefe: restTiefen) = (if tiefe < head restTiefen then 1 else 0) + aoc_loesung_1_1_0 restTiefen
        vergleiche _ = 0


aoc_loesung_1_2_0:: [Tiefe] -> Int
-- erzeuge erst alle fenster vergleiche sie dann mit dem algorhitmus aus der voriegen aufgabe
aoc_loesung_1_2_0 tiefen = aoc_loesung_1_1_0 (summiere tiefen)
    where
        -- sumiere ein fenster zusammen
        summiere:: [Tiefe] -> [Tiefe]
        summiere (erste: (zweite: (dritte: rest))) = (erste + zweite + dritte): summiere (zweite: dritte: rest)
        summiere _ = []


{-  
    Lösung der zweiten Aufgabe im Adventskalender 
-}
aoc_loesung_2_1_0:: Position -> [Positionswechsel] -> Position
aoc_loesung_2_1_0 position (head: tail) = aoc_loesung_2_1_0 (setPosition position head) tail
    where
        setPosition:: Position -> Positionswechsel -> Position
        setPosition alt kurs
            | richtung kurs == Forward = Position (x alt + wert kurs) (y alt) (aim alt)
            | richtung kurs == Down = Position (x alt) (y alt + wert kurs) (aim alt)
            | richtung kurs == Up = Position (x alt) (y alt - wert kurs) (aim alt)
            | otherwise = alt
aoc_loesung_2_1_0 position _ = position


aoc_loesung_2_2_0:: Position -> [Positionswechsel] -> Position
aoc_loesung_2_2_0 position (head: tail) = aoc_loesung_2_2_0 (setPosition position head) tail
    where
        setPosition:: Position -> Positionswechsel -> Position
        setPosition alt kurs
            | richtung kurs == Forward = Position (x alt + wert kurs) (y alt + (aim alt * wert kurs)) (aim alt)
            | richtung kurs == Down = Position (x alt) (y alt) (aim alt + wert kurs)
            | richtung kurs == Up = Position (x alt) (y alt) (aim alt - wert kurs)
            | otherwise = alt
aoc_loesung_2_2_0 position _ = position


{-  
    Lösung der dritten Aufgabe im Adventskalender 
-}
aoc_loesung_3_1_0:: [String] -> Messwert
aoc_loesung_3_1_0 [] = error "no data"
aoc_loesung_3_1_0 bitWords = do
    let gamma = generiereErgebnis (calculateAllCrosssums bitWords 0 (getMaxLength bitWords 0))
    Messwert gamma (invertiere gamma)
    where
        generiereErgebnis:: [Int] -> [Char]
        generiereErgebnis [] = []
        generiereErgebnis (x: xs) = (if length bitWords `div` 2 > x then '1' else '0'): generiereErgebnis xs

        invertiere:: String -> String
        invertiere [] = []
        invertiere (x: xs) = (if x == '1' then '0' else '1'): invertiere xs

        -- Berechnet die maximale länge eines Strings innerhalb eines array aus Strings
        getMaxLength:: [String] -> Int -> Int
        getMaxLength [] finalLength = finalLength
        getMaxLength (y: ys) lastMaxLength = getMaxLength ys (if length y > lastMaxLength then length y else lastMaxLength)

        -- berechnet eine liste aus quersummen für alle indizes
        calculateAllCrosssums:: [String] -> Int -> Int -> [Int]
        calculateAllCrosssums [] _ _ = []
        calculateAllCrosssums arr index limit
            | index < limit = calculateCrosssum arr index: calculateAllCrosssums arr (index + 1) limit
            | otherwise = []
        
        -- berechnet die quersumme für einen Index
        calculateCrosssum:: [String] -> Int -> Int
        calculateCrosssum [] _ = 0
        calculateCrosssum (z: zs) currentIndex = (if z!!currentIndex == '1' then 1 else 0) + calculateCrosssum zs currentIndex


aoc_loesung_3_2_0:: [String] -> (String, String)
aoc_loesung_3_2_0 bitWords = (filterToLast (>=) bitWords 0, filterToLast (<) bitWords 0)
    where
        --filter
        filterToLast:: (Int -> Int -> Bool) -> [String] -> Int -> String
        filterToLast comp arr index 
            | length arr == 1 = head arr 
            | otherwise = filterToLast comp (filterArray arr index (bestimmeFilter comp arr index 0 0)) (index + 1)

        -- berechne paritätsbit
        bestimmeFilter:: (Int -> Int -> Bool) -> [String] -> Int -> Int -> Int -> Char
        bestimmeFilter compFunction [] _ c1 c0 = if compFunction c1 c0 then '1' else '0'
        bestimmeFilter compFunction (x: xs) index c1 c0 = 
            if x!!index == '1' 
                then bestimmeFilter compFunction xs index (c1 + 1) c0 
                else bestimmeFilter compFunction xs index c1 (c0 + 1)

        -- filter alles was unter paritär ist
        filterArray:: [String] -> Int -> Char -> [String]
        filterArray x index criteria = filter (\y -> y!!index == criteria) x

