module Model (
    Tiefe(..),
    Richtung(..),
    Positionswechsel(..),
    Position(..),
    Messwert(..)
) where

type Tiefe = Int
data Richtung = Down | Forward | Up deriving(Eq, Show)
data Positionswechsel = Positionswechsel {richtung:: Richtung, wert:: Int} deriving(Eq, Show)
data Position = Position {x:: Int, y:: Int, aim:: Int} deriving(Eq, Show)
data Messwert = Messwert {gamma:: String, beta:: String} deriving(Eq, Show)
